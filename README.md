Linux/Windows lock screen from terminal.

This project uses [robot.js](https://github.com/octalmage/robotjs), see for installation requirements.

## Install

`npm i -g @jakeklassen/afk`

## Run

`afk`


## Limitations

Not Mac ready yet.
